<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

header("Content-Type: text/html");
require_once('./app/includes/functions.php');
require_once('./app/php_vars.php');
$urlSegments = array_filter( getUriSegments(), 'removeBlank');
$file = getTemplateFile($urlSegments);
$count = count($urlSegments);


// include header

include('parts/headers/default.php');


if(!$count) // check if there are any URL segments, if not - we are on home.php
{
	$homeFile = './pages/home.php';
	echo '<!-- Used file ' .$homeFile. '-->' ;
	include($homeFile);
}
elseif(file_exists('./'.$file)) // look for the file?
{
	echo '<!-- Used file ' .$file. '-->' ;
	include($file);
}
else // fail to 404 page
{
	echo '<!-- Looking for file ' .$file. '-->' ;
	include('pages/404.php');
}

// include footer

include('parts/footers/default.php');
