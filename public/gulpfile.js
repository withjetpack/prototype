var gulp = require('gulp');
var rsync = require('rsyncwrapper').rsync;
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var notify = require("gulp-notify") ;
var bower = require('gulp-bower');
var less = require('gulp-less');
var sass = require('gulp-ruby-sass');

var paths = {
  scripts: ['./bower_components/jquery/dist/jquery.js' , './bower_components/bootstrap/js/*.js' , './assets/js/plugins/*.js' , './assets/js/script.js'],
  images: './assets/images/**/*',
  fonts: ['./bower_components/bootstrap/fonts/**.*' ,'./bower_components/font-awesome/fonts/**.*'],
  less: ['./assets/less/*', './assets/less/*.*'],
  sassPath: './assets/sass',
   bowerDir: './bower_components' 
};



// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function(cb) {
  // You can use multiple globbing patterns as you would with `gulp.src`
  del(['build'], cb);
});

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(paths.bowerDir)) 
});

gulp.task('icons', function() { 
    return gulp.src(paths.fonts) 
        .pipe(gulp.dest('./assets/fonts')); 
});

gulp.task('less', function(){
    return gulp.src(paths.less)
        .pipe(less())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass', function() { 
    return gulp.src(paths.sassPath + '/style.scss')
         .pipe(sass({
             style: 'compressed',
             loadPath: [
                 './assets/sass',
                 paths.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                 paths.bowerDir + '/font-awesome/scss',
             ]
         }) 
            .on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             }))) 
         .pipe(gulp.dest('./assets/css')); 
});

gulp.task('scripts', function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
      .pipe(uglify())
      .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets/js'));
});

// Copy all static images
gulp.task('images', function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('build/img'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.less, ['less']);

});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'images', 'less']);


// sysnc files with source
gulp.task('sync', function() {
  rsync({
    ssh: true,
    src: 'roy@storage:~/fw/',
    dest: './test/',
    recursive: true,
    //syncDest: true,
    args: ['--verbose']
  }, function(error, stdout, stderr, cmd) {
      gutil.log(stdout);
  });
});
