        <!-- Full page slide -->
        <link href="/assets/css/jquery.fullPage.css" type="text/css" rel="stylesheet" />
        <script src="/assets/js/jquery.easings.min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.fullPage.min.js"></script>
        
        <div id="fullpage">
            <div class="section hero-panel">
              <h1>Meticulously crafted websites and brands</h1>
              <a href="#section2">See our work</a>
            </div>
            <div class="section fullscreen-grid" id="Grid">
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
            </div>
        </div>
  
        <script>
          $(document).ready(function() {
            if ( $(window).width() > 767) {      
              $('#fullpage').fullpage({
                      anchors: ['section1', 'section2']
                  });
            } 
            else {
              //Add your javascript for small screens here 
            }              
                          
          });
        </script>      
