<section class="fullscreen-six-blocks">
  <!-- Row of 3 -->
  <div class="single-row">
    
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/tech') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/arch') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/people') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    
  </div>
  <!-- /Row of 3 -->
  <!-- Row of 3 -->
  <div class="single-row">
    
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/animals') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/arc') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    <!-- block -->
    <div class="block-wrapper">
      <div class="relative-container">
        <div style=" background: url('https://placeimg.com/640/640/nature') no-repeat center center; background-size: cover" class="block-tint"></div>
        <div class="table-cell">
          <label class="block-title">Work Title</label>
          <a href="#" class="box-button">View project</a>
        </div>
      </div>
    </div>
    <!-- /block -->
    
  </div>
  <!-- /Row of 3 -->
</section>


