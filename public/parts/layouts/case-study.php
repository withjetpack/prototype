        <!-- bxSlider Javascript file -->
        <script src="/assets/js/jquery.bxslider.min.js"></script>
        <link href="/assets/css/jquery.bxslider.css" rel="stylesheet" />
        
        <!-- Picturefill -->
        <script src="/assets/js/picturefill.min.js"></script>
  
        <header>
            <div class="intro-panel-wrapper clientname-hero">
                <div class="panel-container">
                  
                        <!-- The Hero Intro -->
                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="intro-panel-text">
                                <h1><small>Identity Design & Brand Communication</small>Creating a brand for Fibre Connect</h1>
                                <a class="btn btn-primary" href="#">Do something button</a>
                              </div>  
                            </div>
                          </div>
                        </div>
                        <!-- /The Hero Intro -->
                        
                </div>
            </div>
        </header>
        
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-8">
                                  <h2>A strong voice and visual identity</h2>
                                  <p>Fibre Connect is a new company that offers broadband fibre services to the South African market. As one of Vodacom's reselling partners we designed a visual identity and created new communication to introduce the idea of faster internet to consumers and businesses.</p>
                                  <p>The concept: <strong>The speed of now</strong></p>
                              </div>
                              <div class="col-md-4">
                                <img src="/assets/images/small.jpg"
                                     srcset="/assets/images/large.jpg 1200w,
                                             /assets/images/medium.jpg 768w,
                                             /assets/images/small.jpg 480w"
                                     sizes="(min-width: 768px) 20vw,
                                            100vw"
                                     alt="A rad wolf" />
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>The construction of a unique mark</h2>
                                  <p>We worked through many early concepts for the fibre connect logo. Speed, connections and fibre optic concepts were all explored. These concepts lead us to a final mark which is unique, solid and recognisable.</p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
            <div class="graphic-panel-wrapper" style="background: #ddd;">
              <div class="panel-container">
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            60vw"
                     alt="A rad wolf" />
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            60vw"
                     alt="A rad wolf" />
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            60vw"
                     alt="A rad wolf" />
              </div>
            </div>
        </section>
        
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>Some cool heading here</h2>
                                  <p>We worked through many early concepts for the fibre connect logo. Speed, connections and fibre optic concepts were all explored. These concepts lead us to a final mark which is unique, solid and recognisable.</p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
            <div class="graphic-panel-wrapper single" style="background: #333;">
              <div class="panel-container">
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            60vw"
                     alt="A rad wolf" />
              </div>
            </div>
        </section>
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>They call me Major Bumfluff</h2>
                                  <p>We worked through many early concepts for the fibre connect logo. Speed, connections and fibre optic concepts were all explored. These concepts lead us to a final mark which is unique, solid and recognisable.</p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
            <div class="photo-panel-wrapper">
              <div class="panel-container">
                <img class="full" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="100vw"
                     alt="A rad wolf" />
                <img class="half" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
                <img class="half" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </div>
            </div>
        </section>
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>Simple communication about what Fibre Connect offers</h2>
                                  <p>Lead text consectetur adipiscing elit. Cras hendrerit bibendum interdum. Proin dignissim risus in tincidunt porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam semper eleifend purus ut placerat. Nunc in blandit massa. Morbi facilisis nisi ut ex mattis, id ullamcorper tortor aliquet.</p>
                                  <p><a href="#">www.thebumfluffcorp.com</a></p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
            <div class="photo-panel-wrapper">
              <div class="panel-container">
                <img class="third" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            100vw"
                     alt="A rad wolf" />
                <img class="third" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            100vw"
                     alt="A rad wolf" />
                <img class="third" src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 30vw,
                            100vw"
                     alt="A rad wolf" />
              </div>
            </div>
        </section>
        
        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>Simple communication about what Fibre Connect offers</h2>
                                  <p>Lead text consectetur adipiscing elit. Cras hendrerit bibendum interdum. Proin dignissim risus in tincidunt porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam semper eleifend purus ut placerat. Nunc in blandit massa. Morbi facilisis nisi ut ex mattis, id ullamcorper tortor aliquet.</p>
                                  <p><a href="#">www.thebumfluffcorp.com</a></p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        <section>
          <ul class="bxslider case-study">
              <li>
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </li>

              <li>
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </li>

              <li>
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </li>

              <li>
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </li>

              <li>
                <img src="/assets/images/small.jpg"
                     srcset="/assets/images/large.jpg 1200w,
                             /assets/images/medium.jpg 768w,
                             /assets/images/small.jpg 480w"
                     sizes="(min-width: 768px) 50vw,
                            100vw"
                     alt="A rad wolf" />
              </li>


          </ul>
        </section>

        <section>
        	<div class="info-panel-wrapper">
                  <div class="panel-container">
                      
                          <!-- /Bootstrap Columns -->
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12">
                                  <h2>This is the end</h2>
                                  <p>Lead text consectetur adipiscing elit. Cras hendrerit bibendum interdum. Proin dignissim risus in tincidunt porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam semper eleifend purus ut placerat. Nunc in blandit massa. Morbi facilisis nisi ut ex mattis, id ullamcorper tortor aliquet.</p>
                              </div>
                            </div>
                          </div>
                          <!-- /Bootstrap Columns -->
                          
                  </div>	
            </div>
        </section>
        
        
        <script>
          $('.bxslider').bxSlider({
            pager: false,
            controls: true,
          });  
        </script>
