        <!-- SNAP SVG -->
        <script type="text/javascript" src="/assets/js/snap.svg-min.js"></script>
        
        <!-- Full page slide -->
        <link href="/assets/css/jquery.fullPage.css" type="text/css" rel="stylesheet" />
        <script src="/assets/js/jquery.easings.min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.fullPage.min.js"></script>
        
        <div id="fullpage">
            <div class="section hero-panel">
                
        <svg id="AUX-Shapes" viewBox="-365 -270 1240 1200">
           <path id="A" fill="#6ec829" d="M-92,299.6c14.7-25.8,66.8-119,76-135.6c8.9-16.1,21.7-24.5,33.4-24.5c13.7,0,25.1,7.8,35.2,24.5c15.7,25.8,62,113.1,75.9,135.8c10.3,16.9,10,31.2,3.1,42.5c-6,9.9-17.9,18-38.6,18c-32,0-120.7,0-149.6,0c-21.1,0-31.7-8-37.9-17.2C-102.5,331.6-100.9,315.1-92,299.6z"/>
           <path id="U" fill="#ffb602" d="M255.7,360.1c56.7,0,106.5-48.9,106.5-104.2c0-55.3,0.1-47.2,0.1-71s-20.1-45-45-45c-24.9,0-111.3,0-136.1,0c-24.8,0-45,21.1-45,45s0.1,14.1,0.1,71c0,56.9,47.6,103.8,107.2,104.3C247.3,360.2,250.2,360.1,255.7,360.1z"/>
           <path id="X" fill="#00b3e6" d="M424.9,360.1c-8.8,0-21.6-2.2-32-12.6c-10.6-10.5-12.8-23.3-12.9-32.2c-0.1-15.6,7.1-31.8,18.6-43.4
	c11.5-11.6,21.6-21.8,21.6-21.8s-11.8-11.6-21.7-21.5c-9.9-10-17.6-24.7-18.7-39.1c-1.1-14.2,3.2-27.1,12.4-36.4
	c10.5-10.6,23.4-12.9,32.4-12.9c15.5,0,30.8,6.3,43.1,18.6c12.4,12.3,21.7,21.6,21.7,21.6s9.6-9.7,21.6-21.7
	c11.9-12,27.8-18.9,43.4-18.9c8.8,0,21.6,2.2,32.1,12.6c18.9,18.8,15.9,53.7-5.7,75.5s-21.6,21.8-21.6,21.8s-1.3-1.6,21.7,21.5
	c23,23.1,25.1,56.6,6.3,75.5c-10.5,10.7-23.4,12.9-32.4,12.9c-15.5,0-30.9-6.5-43.1-18.6c-12.2-12.1-21.8-21.6-21.8-21.6
	s-9.3,9.6-21.6,21.7C456.1,353.4,440.6,360.1,424.9,360.1z"/>
           
        
        </svg>
        
        <h1 class="weareaux">We are Auxiliary</h1>
        <h1 class="userfriendly" style="display: none;">The user-friendly design studio</h1>
        <div style="padding: 15px; background: red; display: inline-block; position: absolute; top: 500px; left: 20px; color: #FFF;" class="go">Smile 1</div>
        <div style="padding: 15px; background: red; display: inline-block; position: absolute; top: 500px; left: 20px; color: #FFF; display: none;" class="goAgain">Smile 2</div>
        <div style="padding: 15px; background: red; display: inline-block; position: absolute; top: 500px; left: 20px; color: #FFF; display: none;" class="lastGo">Back to AUX logo</div>
    
        <script>
            $(document).ready(function(){

                $('.go').click(function(){

                    $(this).fadeOut("fast");
                    $(".goAgain").fadeIn("fast");
                    
                    $(".weareaux").fadeOut("fast");
                    $(".userfriendly").fadeIn("fast");

                    var s = Snap("#AUX-Shapes");
                   
                    var AShape = s.select('#A');
                    AShape.attr(
                    {
                      fill: "#6ec829",
                    });
                    AShape.animate(
                    {
                        d:"M-92,248.3c0-30.9,12.7-54.3,31.6-75.6c19.9-22.5,52.7-33.1,77.8-33.1c21.3,0,50.7,4.4,78.6,31.5c28,27.2,32.8,56.3,32.8,82.1c0,30.1-12.5,51-22.2,63.2c-12.1,15-26,29.6-52,37.9c-21.5,6.9-49.8,7.9-72.2,0c-24.2-8.5-43-25.6-49.5-34.7C-80.1,301.8-92,284.8-92,248.3z",
                        transform: 'r0,250,250,s0.6,t-80,-80',
                    },
                        800, mina.bounce
                    );

                    var UShape = s.select('#U');
                    UShape.attr(
                    {
                      fill: "#ffb602 ",
                    });
                    UShape.animate(
                    {
                        d:"M255.7,307.7c56.7,0.1,106.5-48.9,106.5-104.2c0-1.7,0-9.2,0-11.2c0,0-60.7,0-77.7,0c-24.9,0-51.1,0.1-75.9,0c-17.5,0-72,0-72,0c0,1-0.2,9.8-0.2,11.1c0,56.9,47.6,103.8,107.2,104.3C247.3,307.8,250.2,307.7,255.7,307.7z", 
                        //transform: 'r-20,250,250,s2.5,t-16,40',
                    },
                        600, mina.easeinout
                    );

                    var XShape = s.select('#X');
                    XShape.attr(
                    {
                      fill: "#00b3e6 ",
                    });
                    XShape.animate(
                    {
                        //transform: 'r340,350,140,s0.7,t-160,-200',
                    },
                        600, mina.easeinout
                    );
                });


                $('.goAgain').click(function(){

                    $(this).fadeOut("fast");
                    $(".lastGo").fadeIn("fast");
                    
                    var s = Snap("#AUX-Shapes");
                   
                    var AShape = s.select('#A');
                    AShape.attr(
                    {
                      fill: "#6ec829 ",
                    });
                    AShape.animate(
                    {
                        transform: 'r0,0,0,s0.4,t240,-250',
                    },
                        800, mina.bounce
                    );

                    var UShape = s.select('#U');
                    UShape.attr(
                    {
                      fill: "#ffb602 ",
                    });
                    UShape.animate(
                    {
                        transform: 'r15,0,0,s1,t0,0',
                    },
                        600, mina.easeinout
                    );

                    var XShape = s.select('#X');
                    XShape.attr(
                    {
                      fill: "#00b3e6 ",
                    });
                    XShape.animate(
                    {
                        transform: 'r45,350,140,s0.2,t-700,-80',
                    },
                        600, mina.easeinout
                    );
                });
                $('.lastGo').click(function(){

                    $(this).fadeOut("fast");
                    $(".go").fadeIn("fast");
                    
                    $(".weareaux").fadeIn("fast");
                    $(".userfriendly").fadeOut("fast");

                    var s = Snap("#AUX-Shapes");
                   
                    var AShape = s.select('#A');
                    AShape.attr(
                    {
                      fill: "#6ec829 ",
                    });
                    AShape.animate(
                    {
                        d:"M-92,299.6c14.7-25.8,66.8-119,76-135.6c8.9-16.1,21.7-24.5,33.4-24.5c13.7,0,25.1,7.8,35.2,24.5c15.7,25.8,62,113.1,75.9,135.8c10.3,16.9,10,31.2,3.1,42.5c-6,9.9-17.9,18-38.6,18c-32,0-120.7,0-149.6,0c-21.1,0-31.7-8-37.9-17.2C-102.5,331.6-100.9,315.1-92,299.6z",
                        transform: 'r0,0,0,s1,t0,0',
                    },
                        800, mina.bounce
                    );

                    var UShape = s.select('#U');
                    UShape.attr(
                    {
                      fill: "#ffb602 ",
                    });
                    UShape.animate(
                    {
                        d:"M255.7,360.1c56.7,0,106.5-48.9,106.5-104.2c0-55.3,0.1-47.2,0.1-71s-20.1-45-45-45c-24.9,0-111.3,0-136.1,0c-24.8,0-45,21.1-45,45s0.1,14.1,0.1,71c0,56.9,47.6,103.8,107.2,104.3C247.3,360.2,250.2,360.1,255.7,360.1z", 
                        transform: 'r0,0,0,s1,t0,0',
                    },
                        600, mina.easeinout
                    );

                    var XShape = s.select('#X');
                    XShape.attr(
                    {
                      fill: "#00b3e6 ",
                    });
                    XShape.animate(
                    {
                        transform: 'r360,350,140,s1,t0,0',
                    },
                        600, mina.easeinout
                    );
                });
            });  
        </script>        
        
        
        


              <a class="our-work" href="#section2">See our work</a>
            </div>
            
            <div class="section fullscreen-grid" id="Grid">
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
              <a class="block-wrapper" href="#">
                <div class="table-wrap">
                  <div class="cell-wrap">
                    <label>Work Categories</label>
                    <h2>Client Name</h2>
                    <button class="btn btn-default" type="submit">Button</button>
                  </div>
                </div>
              </a>
            </div>
        </div>
  
        <script>
          $(document).ready(function() {
            if ( $(window).width() > 767) {      
              $('#fullpage').fullpage({
                      anchors: ['section1', 'section2']
                  });
            } 
            else {
              //Add your javascript for small screens here 
            }              
                          
          });
        </script>
        
        
        
        
        
        
        
        
        
        
        
        
        
        

