<!-- Contact Details -->
<section>
    <div id="contactDetails">
	<div class="table-wrapper">
	    <div class="inner">
		<div class="company-contact-details">
		   <div class="container-fluid">
		    <div class="row">
			<div class="col-md-12">
			    <h1>Contact Us</h1>
			</div>
		    </div>
		    <div class="row">
			<div class="contact-detail-single">
			    <div class="col-md-3">
				Email us
			    </div>
			    <div class="col-md-9">
				<a href="mailto:name@email.com">name@email.com</a>
			    </div>
			</div>
		    </div>
		    <div class="row">
			<div class="contact-detail-single">
			    <div class="col-md-3">
				Call us
			    </div>
			    <div class="col-md-9">
				<a href="tel:27214487167">+27 21 448 7167</a>
			    </div>
			</div>
		    </div>
		    <div class="row">
			<div class="contact-detail-single">
			    <div class="col-md-3">
				Find us
			    </div>
			    <div class="col-md-9">
				<a href="">
				    37 Station Road,<br />
				    Observatory, 7925<br />
				    Cape Town, South Africa
				</a>
			    </div>
			</div>
		    </div>
		   </div>
		</div>
	    </div>
	</div>
    </div>
</section>
<!-- /Contact Details -->

<!-- Google Map -->
<section>
    <div id="contactMap">
	<iframe width="100%" height="99%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Sploosh%20Digital%2C%20Station%20Road%2C%20Cape%20Town%2C%20Western%20Cape%2C%20South%20Africa&key=AIzaSyAHkdZEFJnEhN8KdwpUaCSriOU8ZwxsKdM"></iframe>	
    </div>
</section>
<!-- /Google Map -->
