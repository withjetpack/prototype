<?php

/* functions */

function getUriSegments() {
    return explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
}
 
function getUriSegment($n) {
    $segs = getUriSegments();
    return count($segs)>0&&count($segs)>=($n-1)?$segs[$n]:'';
}

function getTemplateFile($urlArray = 0)
{
	if($urlArray)
	{
		$path = 'pages/';
		//$cleanurlArray = array_filter( $urlArray, 'removeBlank');
		$totalCount = count($urlArray);
		$count = count($urlArray);

		foreach($urlArray as $seg)
		{
			if($seg )
			{
				$path .= $seg;
				$count--;
				if($count>=1 && $totalCount != 1)
				{
					$path .= '-';
				}
			}	
		}
		$path .= '.php';
		return $path;
	}
}

function removeBlank($data)
{
	if($data) return true;
}

function part($directory, $filename)
{
	include('parts'.'/'.$directory.'/'.$filename.'.php');
}